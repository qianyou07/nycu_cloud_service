from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from flask import Flask
import json
import re
import time
import mysql.connector

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template
from email.mime.image import MIMEImage
from pathlib import Path

crawlTimeRange = (60,'分鐘')
region = {'TaipeiCity':1,'NewTaipeiCity':3,'TaoyuanCity':6,'HsinchuCity':4,'HsinchuCounty':5,'YilanCounty':21,'KeelungCity':2,'TaichungCity':8,'ChanghuaCounty':10,'YunlinCounty':14,'MiaoliCounty':7,'NantouCounty':11,'KaohsiungCity':17,'TainanCity':15,'ChiayiCity':12,'ChiayiCounty':13,'PingtungCounty':19,'TaitungCounty':22,'HualienCounty':23,'PenghuCounty':24,'KinmenCounty':25,'LienchiangCounty':26}

kind = {'整層住家':1,'獨立套房':2,'分租套房':3,'雅房':4,'車位':8,'其他':24}

# check if it's posted within crawlTimeRange
def timeInRange(time_msg):
	if crawlTimeRange[1] == '小時':
		return (int(re.findall('\d+', time_msg)[0]) <= 60 and time_msg.find('分鐘') != -1) or (int(re.findall('\d+', time_msg)[0]) <= crawlTimeRange[0] and time_msg.find('小時') != -1)
	else:
		return int(re.findall('\d+', time_msg)[0]) <= crawlTimeRange[0] and time_msg.find(crawlTimeRange[1]) != -1

def generateURL(keywords,condition):
	url = "https://rent.591.com.tw/"
	url += "?"
	url += "keywords="
	for i in range(len(keywords)):
		if i >= 1:
			url += "%20"
		url += keywords[i]
	url += "&region=" + condition['region']
	if condition['kind']:
		url += "&kind=" + condition['kind']
	if condition['rentprice']:
		url += "&rentprice=" + condition['rentprice']

	url += "&order=posttime&orderType=desc" # 顯示最新貼文
	return url


def crawl(keywords,condition):
	option = webdriver.ChromeOptions()
	# 讓網頁瀏覽器在背景執行
	# option.add_argument("headless")
	# 不加載圖片
	prefs = {"profile.managed_default_content_settings.images": 2}
	option.add_experimental_option("prefs", prefs)

	s = Service("./chromedriver")
	# 透過Browser Driver 開啟 Chrome
	driver = webdriver.Chrome(service=s,options=option)
	# 前往特定網址
	default_url = generateURL(keywords,condition)
	driver.implicitly_wait(5) # wait for loading of the webpage
	driver.get(default_url)

	crawledData = list(dict())
	try:
		find = driver.find_element(By.CLASS_NAME,'content-left-not')
		title = driver.find_elements(By.CLASS_NAME,'item-title')
		tags = driver.find_elements(By.CLASS_NAME,'item-tags')
		style = driver.find_elements(By.CLASS_NAME,'item-style')
		area = driver.find_elements(By.CLASS_NAME,'item-area')
		msg = driver.find_elements(By.CLASS_NAME,'item-msg')
		price = driver.find_elements(By.CLASS_NAME,'item-price')
		nextPageFlag = True

		#find no results
		find = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "content-left-not"))).value_of_css_property("display")
		print(find)
		if find == "block":
			return None
		# if find.value_of_css_property("display") == "block":
		# 	return None

		while True:
			for i in range(len(title)):
				if timeInRange(msg[i].text):
				# if int(re.findall('\d+', msg[i].text)[0]) <= crawlTimeRange[0] and msg[i].text.find(crawlTimeRange[1]) != -1: 
					crawledData.append(dict())
					crawledData[-1]['title'] = title[i].text
					crawledData[-1]['tags'] = tags[i].text
					styleArr = style[i].text.split(' ',1)
					crawledData[-1]['kind'] = str(kind[styleArr[0]])
					crawledData[-1]['style'] = styleArr[1]
					crawledData[-1]['area'] = area[i].text
					crawledData[-1]['msg'] = msg[i].text
					crawledData[-1]['price'] = int(price[i].text[:-3].replace(",",""))
					xpath = '//div[@class="switch-list-content"]/section[' + str(i+1) + ']/a'
					crawledData[-1]['url'] = driver.find_element(By.XPATH,xpath).get_attribute('href')
					img_xpath = xpath + '/div[1]/div[1]/div[1]/ul[1]/li/img'
					crawledData[-1]['imgs'] = list()
					img_source = driver.find_elements(By.XPATH,img_xpath)
					for img in img_source:
						crawledData[-1]['imgs'].append(img.get_attribute('data-original'))
					crawledData[-1]['imgs'] = json.dumps(crawledData[-1]['imgs'],ensure_ascii=False)
					
				else:
					nextPageFlag = False
					break

			if not nextPageFlag:
				break

			try:
				driver.find_element(By.CLASS_NAME,'pageNext').click()
				time.sleep(1)
			except:
				break
	finally:
		driver.quit()
		return crawledData



if __name__ == '__main__':
	
	
	#連接mysql
	mydb = mysql.connector.connect(
		host="localhost",
		user="root",
		password="00000000",
		database="laravel",
		auth_plugin='mysql_native_password'
	)

	#建立信箱列表
	mycursor = mydb.cursor()
	email_sql = "SELECT id,email FROM users WHERE id IN (SELECT user_id FROM queries WHERE user_id IN (SELECT id FROM users WHERE subscribed = '1'));"
	mycursor.execute(email_sql)
	results = mycursor.fetchall()
	userMailMap = {}
	for row in results:
		userMailMap[row[0]] = row[1]

	#從mysql取得關鍵字資料
	keyword_sql = "SELECT keywords,user_id,region,kind,minPrice,maxPrice,id FROM queries WHERE user_id IN (SELECT id FROM users WHERE subscribed = '1');"

	mycursor.execute(keyword_sql)
	results = mycursor.fetchall()

	mailSet = set()
	#爬資料
	for row in results:
		print(row[0],row[1],row[2],row[3],row[4],row[5],row[6])
		keywords = row[0].split(' ')
		condition = dict()
		condition['region'] = str(region[row[2]]) # (required)
		condition['kind'] = row[3] # (optional) 1:整層住家 2:獨立套房 3:分租套房 4:雅房 8:車位 24:其他
		if row[4] or row[5]:
			min = str(row[4]) if row[4] else ""
			max = str(row[5]) if row[5] else ""
			condition['rentprice'] = min + "," + max # (optional) format:[lower limit],[upper limit]
		else:
			condition['rentprice'] = "" # (optional) format:[lower limit],[upper limit]

		crawledData = crawl(keywords,condition)
		
		# print(crawledData)
	#將爬到的資料存回資料庫
		if crawledData:
			for data in crawledData:
				data['query_id'] = int(row[6])
			crawledData = [tuple(d.values()) for d in crawledData]
			print(crawledData)
			insert_sql = '''INSERT INTO houses (title, tags, kind, style, area, msg, price, url, imgs, query_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'''
			mycursor.executemany(insert_sql, crawledData)
			mydb.commit()
			mailSet.add(userMailMap[row[1]])
	
	mailString = ""
	for mail in mailSet:
		mailString += mail + ','

	mailString = mailString[:-1]
	
	content = MIMEMultipart()  #建立MIMEMultipart物件
	content["subject"] = "Di Fun Mama is needing you"  #郵件標題
	content["from"] = "henrylin890511@gmail.com"  #寄件者
	content["to"] = mailString #收件者 用 ',' 分隔 join 起來就可以寄給多人

	template = Template(Path("mail_template.html").read_text())
	body = template.substitute({ "user": "henry" })
	content.attach(MIMEText(body, "html"))

	import smtplib
	with smtplib.SMTP(host="smtp.gmail.com", port="587") as smtp:  # 設定SMTP伺服器
		try:
		
			smtp.ehlo()  # 驗證SMTP伺服器
			smtp.starttls()  # 建立加密傳輸
			smtp.login("henrylin890511@gmail.com", "hhiggmjrntegsfso")  # 登入寄件者gmail
			smtp.send_message(content)  # 寄送郵件


			print("Complete!")
		except Exception as e:
			print("Error message: ", e)



"""
	app = Flask(__name__)

	@app.route("/")
	def home():
		keywords = ['東區']
		condition = dict()
		condition['region'] = str(region['HsinchuCity'])
		condition['kind'] = ""
		condition['rentprice'] = ""
		crawledDataJson = crawl(keywords,condition)
		return json.dumps(crawledDataJson,ensure_ascii=False)

	app.run()
	"""