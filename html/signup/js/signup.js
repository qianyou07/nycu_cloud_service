var form = document.getElementById('login-form');
form.onsubmit = function(event){
	var xhr = new XMLHttpRequest();
	var formData = new FormData(form);
	//open the request
	xhr.open('POST','/api/signup/')
	xhr.setRequestHeader("Content-Type", "application/json");

	//send the form data
	xhr.send(JSON.stringify(Object.fromEntries(formData)));

	xhr.onreadystatechange = function() {
		if (xhr.readyState == XMLHttpRequest.DONE) {
			var callbackData = JSON.parse(xhr.responseText); //因為輸入資料目前是字串，要轉成物件才能使用
			var succeed = callbackData.boolean;
			if (succeed) {
				form.reset(); //reset form after AJAX success or do something else
				alert("帳號註冊成功");
				window.location.href = "/signin/";
			}
			else {
				alert("該Email已被使用");
			}
		}
	}
	//Fail the onsubmit to avoid page refresh.
	return false; 
}