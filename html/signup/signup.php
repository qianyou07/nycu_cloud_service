<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Notify</title>
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

</head>
<body>

	<nav class="top-menu">
		<ul class="top-menu-list">
			<li><a class="home-icon" href="/"><img src="images/homeIcon.png" alt="home icon"></a></li>
			<li><a class="top-menu-item" href="#"><span class="top-menu-item-text">ABOUT</span></a></li>
			<li><a class="top-menu-item" href="#"><span class="top-menu-item-text">ACCOUNT</span></a></li>
			<li><a class="top-menu-item" href="/signup/"><span class="top-menu-item-text">SIGN UP</span></a></li>
		</ul>
	</nav>
	<div class="login-block">
		<h1 class="login-header">Join us!</h1>
		<form class="login-form" id="login-form">
			<div class="email-section">
				<img src="images/email.png" alt="email icon">
				<input type="email" name="email" id="email" placeholder="Email" required>
			</div>
			<div class="password-section">
				<img src="images/password.png" alt="password icon">
				<input type="password" name="password" id="password" placeholder="Password" required>
				<i class="bi bi-eye-slash" id="togglePassword"></i>
			</div>
			<div class="dont-have-account"><a href="/signin/">Already have an account?</a></div>
			<button type="submit" class="login-button">CREATE ACCOUNT</button>
		</form>
	</div>
	<script src="js/togglePassword.js"></script>
	<script src="js/signup.js"></script>
</body>
</html>

