<?php 
//start recoding session
session_start();
//include autoload.php file
require 'vendor/autoload.php';
// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
$input = json_decode(file_get_contents('php://input'),true);

// print_r($request);

//Load data in .env file
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$db_servername = $_ENV['servername'];
$db_username = $_ENV['username'];
$db_password = $_ENV['password'];
$db_dbname = $_ENV['dbname'];

$conn = new mysqli($db_servername,$db_username,$db_password,$db_dbname);

header("Content-Type: application/json");

if($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

$return = array();
//Signup
if(count($request) == 2 && $request[1] == 'signup' && $method == 'POST'){
	if(is_null($input['email']) || is_null($input['password'])) {
		$return = array("code"=>"Requirement: Email,Password in Json","boolean"=>false);
	}
	else {
		$email = $input['email'];
		$password = $input['password'];
		// $query = "SELECT COUNT(*) FROM client WHERE email='".$email."';";
		$query = "INSERT INTO client (email,password,subscribed) VALUES ('".$email."','".$password."',false);";
		$result = $conn->query($query);
		if(!$result){
			$return = array("code"=>"Email repeated.","boolean"=>false);
		}
		else{
			$return = array("code"=>"Sign up succeed.","boolean"=>true);
		}
	}
}
//Signin
else if(count($request) == 2 && $request[1] == 'signin' && $method == 'POST'){
	if(is_null($input['email']) || is_null($input['password'])) {
		$return = array("code"=>"Requirement: Email,Password in Json","boolean"=>false);
	}
	else {
		$email = $input['email'];
		$password = $input['password'];
		$query = "SELECT password FROM client WHERE email='".$email."';";
		$result = $conn->query($query);
		$row_num = mysqli_num_rows($result);
		if(!$result){
			$return = array("code"=>$conn->error,"boolean"=>false);
		}
		else if($row_num == 0){
			$return = array("code"=>"Non-exist email.","boolean"=>false);
		}
		else if($result->fetch_row()[0] !== $password){
			$return = array("code"=>"Wrong password.","boolean"=>false);
		}
		else{
			$return = array("code"=>"Sign in succeed.","boolean"=>true);
			$_SESSION['email'] = $email;
		}
	}
	
}
else if(count($request) == 2 && $request[1] == 'account' && $method == 'GET'){
	if(isset($_SESSION['email'])){
		$email = $_SESSION['email'];
		$query = "SELECT email,subscribed,expiration_date FROM client WHERE email='".$email."';";
		$result = $conn->query($query);
		$data = $result->fetch_assoc();
		$return = array("code"=>"Get account succeed.","boolean"=>true,"data"=>$data);
	}
	else{
		$return = array("code"=>"Sign in to see.","boolean"=>false,"data"=>array());
	}
}
else if(count($request) == 2 && $request[1] == 'subscribe' && $method == 'GET'){
	if(isset($_SESSION['email'])){
		$expiration_date = new DateTime();
		$expiration_date->add(new DateInterval('P1M'));
		$email = $_SESSION['email'];
		$query = "UPDATE client SET subscribed = 1,expiration_date = '".$expiration_date->format('Y-m-d')."' WHERE email='".$email."';";
		$result = $conn->query($query);
		$return = array("code"=>"Subscription succeed.","boolean"=>true,"data"=>array());
		// $data = $result->fetch_row();
		// $return = array("email"=>$data[0],"subscribed"=>$data[1],"expiration_data"=>$data[2]);
	}
	else{
		$return = array("code"=>"Sign in to subscribe.","boolean"=>false,"data"=>array());
	}
}
else{
	$return = array("code"=>"No corresponding api.","boolean"=>false);
}

echo json_encode($return);
$conn->close(); 

?>