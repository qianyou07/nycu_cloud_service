<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Notify</title>
	<link rel="stylesheet" href="styles/style.css">

</head>
<body>
	<nav class="top-menu">
		<ul class="top-menu-list">
			<li><a class="home-icon" href="/"><img src="images/homeIcon.png" alt="home icon"></a></li>
			<li><a class="top-menu-item" href="#"><span class="top-menu-item-text">ABOUT</span></a></li>
			<li><a class="top-menu-item" href="/api/account/"><span class="top-menu-item-text">ACCOUNT</span></a></li>
			<li><?php 
			if(isset($_SESSION['email'])){
				echo '<a class="top-menu-item" href="signout.php"><span class="top-menu-item-text">SIGN OUT</span></a>';
			}
			else{
				echo '<a class="top-menu-item" href="/signup/"><span class="top-menu-item-text">SIGN UP</span></a>';
			}
			?></li>
		</ul>
	</nav>
	<div></div>
	<div class="main-block">
		<img src="images/mainIcon.png" alt="main icon">
		<div class="subscribe-area">
			<img src="images/bottomLine.png" id="bottomLine0">
			<?php
				if(isset($_SESSION['email'])){
					echo '<a class="subscribe" href="/api/subscribe/">';
				}
				else{
					echo '<a class="subscribe" href="/signin/">';
				}
			?>
			<!-- <a class="subscribe" href="/api/subscribe/"> -->
				<img src="images/subscribeBtn.png" alt="subscribe btn">
			</a>
			<img src="images/bottomLine.png" id="bottomLine1">
		</div>
	</div>
</body>
</html>
