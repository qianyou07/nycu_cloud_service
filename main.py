from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re
import time

crawlTimeRange = (5,'分鐘')
region = {'TaipeiCity':1,'NewTaipeiCity':3,'TaoyuanCity':6,'HsinchuCity':4,'HsinchuCounty':5,'YilanCounty':21,'KeelungCity':2,'TaichungCity':8,'ChanghuaCounty':10,'YunlinCounty':14,'MiaoliCounty':7,'NantouCounty':11,'KaohsiungCity':17,'TainanCity':15,'ChiayiCity':12,'ChiayiCounty':13,'PingtungCounty':19,'TaitungCounty':22,'HualienCounty':23,'PenghuCounty':24,'KinmenCounty':25,'LienchiangCounty':26}

def generateURL(keywords,condition):
	url = "https://rent.591.com.tw/"
	url += "?"
	url += "keywords="
	for i in range(len(keywords)):
		if i >= 1:
			url += "%20"
		url += keywords[i]
	url += "&region=" + condition['region']
	if condition['kind']:
		url += "&kind=" + condition['kind']
	if condition['multiPrice']:
		url += "&multiPrice=" + condition['multiPrice']

	url += "&order=posttime&orderType=desc" # 顯示最新貼文
	return url


def crawl(keywords,condition):
	s = Service("./chromedriver")
	# 透過Browser Driver 開啟 Chrome
	driver = webdriver.Chrome(service=s)
	# 前往特定網址
	default_url = generateURL(keywords,condition)
	driver.implicitly_wait(10) # wait for loading of the webpage
	driver.get(default_url)

	crawledData = list(dict())
	try:
		title = driver.find_elements(By.CLASS_NAME,'item-title')
		tags = driver.find_elements(By.CLASS_NAME,'item-tags')
		style = driver.find_elements(By.CLASS_NAME,'item-style')
		area = driver.find_elements(By.CLASS_NAME,'item-area')
		msg = driver.find_elements(By.CLASS_NAME,'item-msg')
		price = driver.find_elements(By.CLASS_NAME,'item-price')
		nextPageFlag = True

		while True:
			for i in range(len(title)):
				if int(re.findall('\d+', msg[i].text)[0]) <= crawlTimeRange[0] and msg[i].text.find(crawlTimeRange[1]) != -1: # check if it's posted within crawlTimeRange
					crawledData.append(dict())
					crawledData[-1]['title'] = title[i].text
					crawledData[-1]['tags'] = tags[i].text
					crawledData[-1]['style'] = style[i].text
					crawledData[-1]['area'] = area[i].text
					crawledData[-1]['msg'] = msg[i].text
					crawledData[-1]['price'] = price[i].text
					xpath = '//div[@class="switch-list-content"]/section[' + str(i+1) + ']/a'
					crawledData[-1]['url'] = driver.find_element(By.XPATH,xpath).get_attribute('href')
					img_xpath = xpath + '/div[1]/div[1]/div[1]/ul[1]/li/img'
					crawledData[-1]['imgs'] = list()
					img_source = driver.find_elements(By.XPATH,img_xpath)
					for img in img_source:
						crawledData[-1]['imgs'].append(img.get_attribute('data-original'))
					print(crawledData[-1])
				else:
					nextPageFlag = False
					break

			if not nextPageFlag:
				break

			try:
				driver.find_element(By.CLASS_NAME,'pageNext').click()
				time.sleep(1)
			except:
				break
	finally:
		driver.quit()
		return



if __name__ == '__main__':
	keywords = ['東區']
	condition = dict()
	condition['region'] = str(region['HsinchuCity']) # (required)
	condition['kind'] = "" # (optional) 1:整層住家 2:獨立套房 3:分租套房 4:雅房 8:車位 24:其他
	condition['multiPrice'] = "" # (optional) format:[lower limit]_[upper limit]
	crawl(keywords,condition)
