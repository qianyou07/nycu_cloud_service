from ..crawler.main import *

def test_generateUrl():
	keywords = ['東區']
	condition = dict()
	condition['region'] = str(region['HsinchuCity']) # (required)
	condition['kind'] = "" # (optional) 1:整層住家 2:獨立套房 3:分租套房 4:雅房 8:車位 24:其他
	condition['multiPrice'] = "" # (optional) format:[lower limit]_[upper limit]
	url = generateURL(keywords,condition)
	assert url == "https://rent.591.com.tw/?keywords=東區&region=4&order=posttime&orderType=desc"

def test_timeInRange():
	assert timeInRange("3分鐘內更新") == True
	assert timeInRange("58分鐘內更新") == False
	assert timeInRange("3小時內更新") == False