import requests
from .conftest import *
import time

host = "http://192.168.0.100:8080/"

def test_signin():
	email = "qq@gmail.com"
	password = "0000"
	url = host + "api/signin/"
	res = requests.post(url,json={"email":email,"password":password})
	response = res.json()
	assert response['boolean'] == True

def test_invalid_signin():
	email = "qq@gmail.com"
	url = host + "api/signin/"
	res = requests.post(url,json={"email":email})
	response = res.json()
	assert response['boolean'] == False

def test_signup():
	email = str(int(time.time()))
	url = host + "api/signup/"
	res = requests.post(url,json={"email":email,"password":"00000000"})
	response = res.json()
	assert response['boolean'] == True

def test_invalid_signup():
	email = str(int(time.time()))
	url = host + "api/signup/"
	res = requests.post(url,json={"email":email})
	response = res.json()
	assert response['boolean'] == False

def test_account(login_default_user):
	url = host + "api/account/"
	res = s.get(url)
	response = res.json()
	assert response['boolean'] == True

def test_subscribe(login_default_user):
	url = host + "api/subscribe/"
	res = s.get(url)
	response = res.json()
	assert response['boolean'] == True