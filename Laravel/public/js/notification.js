var url = new URL(window.location.href);
const region = document.getElementById('region');
const minPrice = document.getElementById('minPrice');
const maxPrice = document.getElementById('maxPrice');
const kind = document.getElementById('kind');
const words = document.getElementsByName('words');
const form = document.getElementById('condition');

if(url.searchParams.has('words')){
	var wordArr = url.searchParams.get('words').split(",");
	words.forEach(checkIfExists);
}

if(!url.searchParams.has('region')){
	url.searchParams.set('region','TaipeiCity');
	window.location.replace(url.href);
}

region.addEventListener('change',function() { editUrl(region.id,region.value)});
minPrice.addEventListener('change',function() { editUrl(minPrice.id,minPrice.value)});
maxPrice.addEventListener('change',function() { editUrl(maxPrice.id,maxPrice.value)});
kind.addEventListener('change',function() { editUrl(kind.id,kind.value)});
words.forEach(element => element.addEventListener('change',function() { editUrl(element.name,element.value)}));
// region.addEventListener('change',function() { form.submit();});
// minPrice.addEventListener('change',function() { form.submit();});
// maxPrice.addEventListener('change',function() { form.submit();});
// kind.addEventListener('change',function() { form.submit();});
// words.forEach(element => element.addEventListener('change',function() { form.submit();}));


function containWord(word){
	if(!url.searchParams.has('words')) return false;
	var wordArr = url.searchParams.get('words').split(",");
	return wordArr.includes(word);
}

function deleteWord(word){
	var wordArr = url.searchParams.get('words').split(",");
	wordArr.splice(wordArr.indexOf(word), 1);
	console.log(wordArr.join(","));
	return wordArr.join(",");
}

function checkIfExists(item){
	if(containWord(item.value)){
		item.checked = true;
	}
}

function editUrl(id,value) {
	if(id === "region"){
		url.searchParams.delete("words");
		url.searchParams.delete("page");
	}
	if(id === "words"){
		if(containWord(value)){
			updatedValue = deleteWord(value);
			if(updatedValue !== "")
				url.searchParams.set(id,deleteWord(value));
			else
				url.searchParams.delete(id);
		} else{
			if(url.searchParams.has(id))
				url.searchParams.set(id,url.searchParams.get(id).concat(",",value));
			else
				url.searchParams.set(id,value);
		}
	} else{
		if(value !== ''){
				url.searchParams.set(id, value);
			// window.location.replace(url);
			// history.pushState(state, title, url);
		} else{
			url.searchParams.delete(id);
		}
	}
	window.location.replace(url.href);
}