const usernameBtn = document.getElementById("usernameBtn");
const passwordBtn = document.getElementById("passwordBtn");
const usernameCrossBtn = document.getElementById("usernameCrossBtn");
const passwordCrossBtn = document.getElementById("passwordCrossBtn");
const usernameForm = document.getElementById("usernameForm");
const passwordForm = document.getElementById("passwordForm");
const username = document.getElementById("username");
const password = document.getElementById("password");
const originUsername = username.value;
const originPassword = password.value;

usernameBtn.addEventListener("click", function() {
	if(!username.disabled) {
		usernameForm.submit();
	}
	else{
		password.value = originPassword;
		usernameBtn.setAttribute('src','imgs/checkIcon.svg');
		username.disabled = false;
		passwordBtn.setAttribute('src','imgs/wrenchIcon.svg');
		password.disabled = true;
		usernameCrossBtn.style.display = "block";
		passwordCrossBtn.style.display = "none";
	}
	
});

usernameCrossBtn.addEventListener("click", function() {
	username.value = originUsername;
	usernameBtn.setAttribute('src','imgs/wrenchIcon.svg');
	username.disabled = true;
	usernameCrossBtn.style.display = "none";
});

passwordBtn.addEventListener("click", function() {
	if(!password.disabled) {
		passwordForm.submit();
	}
	else{
		password.value = "";
		passwordBtn.setAttribute('src','imgs/checkIcon.svg');
		password.disabled = false;
		usernameBtn.setAttribute('src','imgs/wrenchIcon.svg');
		username.disabled = true;
		usernameCrossBtn.style.display = "none";
		passwordCrossBtn.style.display = "block";
	}
	
});

passwordCrossBtn.addEventListener("click", function() {
	password.value = originPassword;
	passwordBtn.setAttribute('src','imgs/wrenchIcon.svg');
	password.disabled = true;
	passwordCrossBtn.style.display = "none";
});