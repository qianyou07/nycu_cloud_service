<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class RoommateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::latest()->paginate(4);
        return view('roommate',[
            'posts' => $posts,
        ]);
    }

    public function show(Post $post){
        return view('roommateInfo',[
            'post' => $post,
        ]);
    }
}
