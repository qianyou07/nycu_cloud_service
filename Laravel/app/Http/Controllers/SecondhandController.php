<?php

namespace App\Http\Controllers;

use App\Models\Secondhand;
use Illuminate\Http\Request;

class SecondhandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // auth()->user()->secondhands()->create([
        //     'img' => 'https://img.ruten.com.tw/s2/8/e9/36/21617604857142_376.jpg',
        //     'title' => '55寸液晶電視',
        //     'price' => 5000,
        //     'description' => '看電影很爽',
        //     'year' => 3
        // ]);
        // dd('success');
        $secondhands = Secondhand::latest()->paginate(4);
        return view('secondhand',[
            'secondhands' => $secondhands,
        ]);
    }

    public function show(Secondhand $secondhand)
    {
        return view('secondhandInfo',[
            'secondhand' => $secondhand,
        ]);
    }
}
