<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('profile');
        // dd(User::all());
        // dd(auth()->user());
    }

    public function update(Request $request){
        
        // dd($request->all());
        $user = User::find(auth()->user()->id);
        if($request->has('username')){
            $request->validate([
                'username' => "required|string|max:255|unique:users,username,{$user->id}",
            ]);
            $user->username = $request->username;
        }
        elseif ($request->has('password')){
            $request->validate([
                'password' => 'required|string|min:8'
            ]);
            $user->password = Hash::make($request->password);
        }
        $user->update();
        return redirect()->back();
    }

    public function subscribe(){
        $user = User::find(auth()->user()->id);
        $user->subscribed = 1;
        $user->expire_at = now()->addMonth(1);
        $user->save();
        // ->update(['subscribed' => 1,'expire_at' => now()->addMonth(1)]);
        return back();
    }
}
