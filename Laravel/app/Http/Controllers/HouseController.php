<?php

namespace App\Http\Controllers;

use App\Models\House;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(House $house){
        // dd($house);
        return view('house.house',[
            "house" => $house,
        ]);
    }
}
