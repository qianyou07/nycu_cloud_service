<?php

namespace App\Http\Controllers;

use App\Models\House;
use App\Models\Query;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $words = explode(',',$request->words);
        $houses = House::whereIn('query_id',$words);
        if ($request->has('minPrice')){
            $houses = $houses->where('price','>=',$request->minPrice);
        }
        if ($request->has('maxPrice')){
            $houses = $houses->where('price','<=',$request->maxPrice);
        }
        if ($request->has('kind')){
            $houses = $houses->where('kind','=',$request->kind);
        }
        $houses = $houses->paginate(4);
        $queries = Query::where('user_id',auth()->id())->get()->where('region',$request->region);

        return view('notification',[
            'queries' => $queries,
            'request' => $request,
            'houses' => $houses,
        ]);
    }

    public function store(Request $request){
        $this->validate($request,[
            'region' => 'required',
            'keywords' => ['required',Rule::unique('queries')->where('user_id',auth()->id())->where('region',$request->region)]
        ]);

        $request->user()->queries()->create([
            'keywords' => $request->keywords,
            'region' => $request->region
        ]);
        
        return back();
    }
}
