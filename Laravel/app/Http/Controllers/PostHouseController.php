<?php

namespace App\Http\Controllers;

use App\Models\House;
use Illuminate\Http\Request;

class PostHouseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(House $house){
        return view('house.post',[
            "house" => $house,
        ]);
    }

    public function store(Request $request,House $house){
        $this->validate($request,[
            'description' => 'required'
        ]);

        $request->user()->posts()->create([
            'house_id' => $house->id,
            'description' => $request->description,
            'people' => $request->people,
        ]);
        return view('house.house',[
            "house" => $house,
        ]);
    }
}
