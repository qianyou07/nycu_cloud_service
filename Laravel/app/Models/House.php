<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    protected $fillable = [
        'query_id',
        // 'data',
        'title',
        'tags',
        'kind',
        'style',
        'area',
        'msg',
        'price',
        'url',
        'imgs',
    ];


    public function posts()
    {
        return $this->hasMany(House::class);
    }
    // public function query()
    // {
    //     return $this->belongsTo(Query::class);
    // }
}
