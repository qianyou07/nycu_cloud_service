<?php

use App\Http\Controllers\HouseController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PostHouseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoommateController;
use App\Http\Controllers\SecondhandController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
Route::post('/profile', [ProfileController::class, 'subscribe']);
Route::patch('/profile', [ProfileController::class, 'update']);

Route::get('/notification', [NotificationController::class, 'index'])->name('notification');
Route::post('/notification', [NotificationController::class, 'store']);

Route::get('/notification/{house}', [HouseController::class, 'index'])->name('notification.house');

Route::get('/notification/{house}/post', [PostHouseController::class, 'index'])->name('notification.house.post');
Route::post('/notification/{house}/post', [PostHouseController::class, 'store']);

Route::get('/roommate', [RoommateController::class, 'index'])->name('roommate');

Route::get('/roommate/{post}', [RoommateController::class, 'show'])->name('roommate.info');

Route::get('/secondhand', [SecondhandController::class, 'index'])->name('secondhand');

Route::get('/secondhand/{secondhand}', [SecondhandController::class, 'show'])->name('secondhand.info');