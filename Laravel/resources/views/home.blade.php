@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/home.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid mt-4">
    <div class="row d-flex justify-content-evenly align-items-center">
        <div class="col-md-3 d-flex mb-md-0 mb-5 justify-content-center align-items-center">
            <a href="{{route('notification')}}">
                <img src="imgs/notifyCard.svg" alt="notifyCard">
                <img src="imgs/notifyCardHover.svg" alt="notifyCard">
            </a>
        </div>
        <div class="col-md-3 d-flex mb-md-0 mb-5 justify-content-center align-items-center">
            <a href="{{route('roommate')}}">
                <img src="imgs/roommateCard.svg" alt="roommateCard">
                <img src="imgs/roommateCardHover.svg" alt="roommateCard">
            </a>
        </div>
        <div class="col-md-3 d-flex justify-content-center">
            <a href="{{route('secondhand')}}">
                <img src="imgs/secondhandCard.svg" alt="secondhandCard">
                <img src="imgs/secondhandCardHover.svg" alt="secondhandCard align-items-center">
            </a>
        </div>
    </div>
</div>
@endsection
