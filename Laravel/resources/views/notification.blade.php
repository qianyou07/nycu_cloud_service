@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/notification.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid px-4">
	<div class="row justify-content-around">
		<div class="col-8 my-4 px-3">
			@foreach ($houses as $house)
				<a href="{{route('notification.house',$house)}}" class="my-3 ps-5 py-3 d-flex">
					<img class="col-2" src="{{json_decode($house->imgs)[0]}}" alt="house image">
					<h4 class="col-6 ps-5">{{$house->title}}</h4>
					<p class="col-4 text-end d-flex justify-content-end align-items-center">${{$house->price}}/month<br>{{$house->area}}</p>
				</a>
				
			@endforeach
			<div class="d-flex justify-content-center">
				{!! $houses->appends($request->all())->links() !!}
			</div>
		</div>
		<div class="col-3 my-4 px-3 d-flex flex-wrap justify-content-center">
			<form class="row justify-content-center" action="{{route('notification')}}" method="post">
				@csrf
				<div class="row justify-content-center">
					<div class="col-12 mt-4 d-flex justify-content-center">
						<select name="region" id="region" class="px-5">
							<option value="TaipeiCity" {{($request->region == "TaipeiCity" ? "selected":"")}}>台北市</option>
							<option value="NewTaipeiCity" {{($request->region == "NewTaipeiCity" ? "selected":"")}}>新北市</option>
							<option value="TaoyuanCity" {{($request->region == "TaoyuanCity" ? "selected":"")}}>桃園市</option>
							<option value="HsinchuCity" {{($request->region == "HsinchuCity" ? "selected":"")}}>新竹市</option>
							<option value="HsinchuCounty" {{($request->region == "HsinchuCounty" ? "selected":"")}}>新竹縣</option>
							<option value="YilanCounty" {{($request->region == "YilanCounty" ? "selected":"")}}>宜蘭縣</option>
							<option value="KeelungCity" {{($request->region == "KeelungCity" ? "selected":"")}}>基隆市</option>
							<option value="TaichungCity" {{($request->region == "TaichungCity" ? "selected":"")}}>台中市</option>
							<option value="ChanghuaCounty" {{($request->region == "ChanghuaCounty" ? "selected":"")}}>彰化縣</option>
							<option value="YunlinCounty" {{($request->region == "YunlinCounty" ? "selected":"")}}>雲林縣</option>
							<option value="MiaoliCounty" {{($request->region == "MiaoliCounty" ? "selected":"")}}>苗栗縣</option>
							<option value="NantouCounty" {{($request->region == "NantouCounty" ? "selected":"")}}>南投縣</option>
							<option value="KaohsiungCity" {{($request->region == "KaohsiungCity" ? "selected":"")}}>高雄市</option>
							<option value="TainanCity" {{($request->region == "TainanCity" ? "selected":"")}}>台南市</option>
							<option value="ChiayiCity" {{($request->region == "ChiayiCity" ? "selected":"")}}>嘉義市</option>
							<option value="ChiayiCounty" {{($request->region == "ChiayiCounty" ? "selected":"")}}>嘉義縣</option>
							<option value="PingtungCounty" {{($request->region == "PingtungCounty" ? "selected":"")}}>屏東縣</option>
							<option value="TaitungCounty" {{($request->region == "TaitungCounty" ? "selected":"")}}>台東縣</option>
							<option value="HualienCounty" {{($request->region == "HualienCounty" ? "selected":"")}}>花蓮縣</option>
							<option value="PenghuCounty" {{($request->region == "PenghuCounty" ? "selected":"")}}>澎湖縣</option>
							<option value="KinmenCounty" {{($request->region == "KinmenCounty" ? "selected":"")}}>金門縣</option>
							<option value="LienchiangCounty" {{($request->region == "LienchiangCounty" ? "selected":"")}}>連江縣</option>
						</select>
					</div>
					<div class="row justify-content-end">
						<div class="col-12 mb-2 d-flex flex-wrap justify-content-end">	
							<input type="text" name="keywords" id="keywords" class="form-control @error('keywords') is-invalid @enderror" autofocus>
							@error('keywords')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="col-12 d-flex justify-content-end">
							<input type="submit" value="Add Keyword">
						</div>
					</div>
				</div>
			</form>
			<form action="{{route('notification')}}" id="condition" method="get">
				@csrf
				<div class="row mt-3 justify-content-center">
					<div class="col-9 d-flex flex-wrap justify-content-evenly">
						@if ($queries->count())
							@foreach ($queries as $query)
								<input type="checkbox" class="btn-check" id="{{$query->id}}" value="{{$query->id}}" name="words" autocomplete="off">
								<label class="btn btn-primary mx-3 my-2" for="{{$query->id}}">{{$query->keywords}}</label>
							@endforeach
						@else
							<p>no queries.</p>
						@endif
					</div>
				</div>
				<div class="row justify-content-center align-items-end">
					<div class="d-flex col-10 my-3 align-items-center py-2">
						<label class="me-auto" for="minPrice">Range</label>
						<input type="number" name="minPrice" id="minPrice" value="{{$request->minPrice}}">
						~
						<input type="number" name="maxPrice" id="maxPrice" value="{{$request->maxPrice}}">
					</div>
					<select class="col-10 my-3 py-2" name="kind" id="kind">
						<option value="" {{($request->kind == "" ? "selected":"")}}>不限</option>
						<option value="1" {{($request->kind == "1" ? "selected":"")}}>整層住家</option>
						<option value="2" {{($request->kind == "2" ? "selected":"")}}>獨立套房</option>
						<option value="3" {{($request->kind == "3" ? "selected":"")}}>分租套房</option>
						<option value="4" {{($request->kind == "4" ? "selected":"")}}>雅房</option>
						<option value="8" {{($request->kind == "8" ? "selected":"")}}>車位</option>
						<option value="24" {{($request->kind == "24" ? "selected":"")}}>其他</option>
					</select>
				</div>
			</form>
		</div>
	</div>
</div>
	
@endsection

@section('scripts')
	<script src="{{ asset('js/notification.js') }}" defer></script>
@endsection
