@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/house/post.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container-fluid">
		<h2 class="col-12 pt-4 pe-5 text-end fw-bold">Post for Roommates</h2>
		<div class="row">
			<a class="col-5 ms-4 mb-4 py-3 text-center" href="{{route('notification.house',$house)}}">
				<h1 class="">{{$house->title}}</h1>
			</a>
		</div>
		<form action="{{route('notification.house.post',$house)}}" class="col-11 d-flex flex-wrap justify-content-end" method="POST">
			@csrf
			<div class="col-6 mb-5">
				<label for="people">Looking for</label>
				<select name="people" id="people" class="mb-3 pe-5 text-end">
					<option value="1">1 person</option>
					<option value="2">2 people</option>
					<option value="3">3 people</option>
					<option value="4">4 people</option>
					<option value="5">5 people</option>
				</select>
			</div>

			<div class="col-10 px-4 pb-4 pt-3 mb-4">
				<label for="description" class="col-12 fs-3 ms-2">Description</label>
				<textarea name="description" id="description" cols="30" rows="8" placeholder="Introduce yourself or describe your expectation about roommates!" class="col-12 form-control @error('description') is-invalid @enderror"></textarea>
				@error('description')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
			</div>
			<input class="offset-10 py-3 mb-5 fs-3" type="submit" value="Post">
		</form>
	</div>
@endsection