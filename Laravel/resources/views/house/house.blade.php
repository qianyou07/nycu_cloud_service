@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/house/house.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container-fluid mt-5">
		<!-- <h2 class="col-12 pt-4 pe-5 text-end fw-bold">House Info.</h2> -->
		<div class="row d-flex justify-content-evenly">
			<div class="col-3">
				<div class="row">
					<div id="slide" class="col carousel slide" data-bs-ride="carousel">
						<div class="carousel-inner">
							@foreach (json_decode($house->imgs) as $key => $img)
							<div class="carousel-item {{($key ===array_key_first(json_decode($house->imgs)))? "active":""}}">
								<img src="{{$img}}" class="d-block w-100">
							</div>
							@endforeach
						</div>
						<button class="carousel-control-prev" type="button" data-bs-target="#slide" data-bs-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="visually-hidden">Previous</span>
						</button>
						<button class="carousel-control-next" type="button" data-bs-target="#slide" data-bs-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="visually-hidden">Next</span>
						</button>
					</div>
				</div>
				<div class="row justify-content-center">
					<a class="col-8 mt-5 py-4 text-center d-flex justify-content-center align-items-center" href="{{route('notification.house.post',$house)}}">
						<h4>Post for roommate</h4>
						<img src="{{asset('imgs/postBtn.svg')}}" alt="post button">
					</a>
				</div>
			</div>
			<div class="col-7 pe-5">
				<div class="row">
					<h1 class="col-8 mb-4 pb-3 text-center">{{$house->title}}</h1>
				</div>
				<div class="row mb-3 d-flex align-items-center">
					<p class="col-3 py-3 ps-4 fs-4">Area</p>
					<p class="col-8 fs-4 text-end">{{$house->area}}</p>
				</div>
				<div class="row mb-3 d-flex align-items-center">
					<p class="col-3 py-3 ps-4 fs-4">Style</p>
					<p class="col-8 fs-4 text-end">{{$house->style}}</p>
				</div>
				<div class="row mb-3 d-flex align-items-center">
					<p class="col-3 py-3 ps-4 fs-4">Price</p>
					<p class="col-8 fs-4 text-end">${{$house->price}}/month</p>
				</div>
				<div class="row mb-3 d-flex align-items-center">
					<p class="col-3 py-3 ps-4 fs-4">Kind</p>
					<p class="col-8 fs-4 text-end">
					@switch($house->kind)
						@case(1)
							整層住家
							@break
						@case(2)
							獨立套房
							@break
						@case(3)
							分租套房
							@break
						@case(4)
							雅房
							@break
						@case(8)
							車位
							@break
						@case(24)
							其他
							@break
						@default
					@endswitch
					</p>
				</div>
				<div class="row mb-3 d-flex align-items-center">
					<p class="col-3 py-3 ps-4 fs-4">Tags</p>
					<p class="col-8 fs-4 text-end">{{$house->tags}}</p>
				</div>
				<div class="row mb-3 d-flex align-items-center">
					<p class="col-3 py-3 ps-4 fs-4">Url</p>
					<a href="{{$house->url}}" class="col-8 fs-4 text-end">{{$house->url}}</a>
				</div>
			</div>
		</div>
	</div>
@endsection