@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/roommate.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container-fluid">
		<h2 class="col-11 py-2 ps-5 mb-4 fw-bold text-end">Finding Roommates</h2>
		<div class="row justify-content-center">
			<div class="col-11 mb-4">
				@foreach ($posts as $post)
					<a href="{{route('roommate.info',$post)}}" class="my-3 ps-5 py-3 d-flex col-12">
						<img class="col-1" src="{{json_decode($post->house()->first()->imgs)[0]}}" alt="house image">
						<h4 class="col-5 ps-5">{{$post->house()->first()->title}}</h4>
						<p class="col-2 text-end d-flex justify-content-end align-items-center">${{$post->house()->first()->price}}/month<br>{{$post->house()->first()->area}}</p>
						<p class="col-4 d-flex justify-content-center align-items-center fs-4 fw-bold">Looking for {{$post->people}} {{Str::plural('roommate',$post->people)}}</p>
					</a>
				@endforeach
				<div class="d-flex justify-content-center">
					{!! $posts->links() !!}
				</div>
			</div>
		</div>
	</div>
@endsection