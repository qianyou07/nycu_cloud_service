@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
	<div class="col">
		<img src="imgs/profileBlueBar.svg" alt="blueBar">
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-4 d-flex align-items-center flex-column">
			<div class="container d-flex justify-content-center border border-dark">
				<img src="imgs/selfie.png" alt="selfie">
			</div>
			<div class="mt-5 text-center">
				<h2>{{  auth()->user()->username }}</h2>
				<p class="text-secondary">{{  auth()->user()->email }}</p>
			</div>
		</div>
		<div class="col-8">
			<div class="card">
				<div class="card-body py-5 d-flex flex-wrap">
					<div class="col col-md-6 px-5 my-4">
						<form id="usernameForm" action=" {{route('profile')}} " method="POST">
							@csrf
							@method('PATCH')
							<div class="d-flex align-items-center">
								<h4 class="text-nowrap">Username</h4>
								<a href="#" class="ms-2">
									<img id="usernameBtn" src="imgs/wrenchIcon.svg" alt="wrenchIcon">
								</a>
								<a id="usernameCrossBtn" href="#">
									<img src="imgs/crossIcon.svg" alt="crossIcon">
								</a>
							</div>
							<input id="username" name="username" class="py-2 ps-2 form-control @error('username') is-invalid @enderror" type="text" value="{{ auth()->user()->username  }}" disabled required autocomplete="username">
							@error('username')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</form>
					</div>
					<div class="col col-md-6 px-5 my-4">
						<h4 class="text-nowrap">Email</h4>
						<input class="py-2 ps-2" type="email" value="{{  auth()->user()->email  }}" disabled>
					</div>
					<div class="col col-md-6 px-5 my-4">
						<form id="passwordForm" action=" {{route('profile')}} " method="POST">
							@csrf
							@method('PATCH')
							<div class="d-flex align-items-center">
								<h4 class="text-nowrap">Password</h4>
								<a href="#" class="ms-2">
									<img id="passwordBtn" src="imgs/wrenchIcon.svg" alt="wrenchIcon">
								</a>
								<a id="passwordCrossBtn" href="#">
									<img src="imgs/crossIcon.svg" alt="crossIcon">
								</a>
							</div>
							<input id="password" name="password" class="py-2 ps-2 form-control @error('password') is-invalid @enderror" type="password" value="{{  auth()->user()->password  }}" disabled required>
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</form>
					</div>
					<div class="col col-md-6 px-5 my-4">
						<h4 class="text-nowrap">State</h4>
						@if (auth()->user()->subscribed)
							<input class="py-2 ps-2" type="text" value="Subscribed" disabled>
						@else
							<input class="py-2 ps-2" type="text" value="Unsubscribed" disabled>
						@endif
					</div>
					<div class="col col-md-6 px-5 my-4">
						<h4 class="text-nowrap">Expiration date</h4>
						@if (auth()->user()->expire_at)
							<input class="py-2 ps-2" type="text" value="{{  auth()->user()->expire_at  }}" disabled>						
						@else
							<input class="py-2 ps-2" type="text" value="Null" disabled>						
						@endif
					</div>
					<div class="col col-md-6 px-5 my-4">
						<h4 class="text-nowrap">Pay for service</h4>
						<form action="{{route('profile')}}" method="POST">
							@csrf
							<a href="#" onclick="this.parentNode.submit()">
								<img src="imgs/payForTheService.svg" alt="payIcon">
							</a>
						</form>
					</div>
	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script src="{{ asset('js/profile.js') }}" defer></script>
@endsection