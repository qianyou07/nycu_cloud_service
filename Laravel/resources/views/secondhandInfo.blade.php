@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/secondhandInfo.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row justify-content-evenly">
			<div class="col-7">
				<div class="row py-4 px-4">
					<div class="col-5">
						<img src="{{$secondhand->img}}" class="d-block w-100">
					</div>
					<div class="col-7">
						<div class="row mb-5">
							<div class="col-10">
								<h1 class="">{{$secondhand->title}}</h1>
							</div>
						</div>
						<div class="row mb-4 justify-content-center">
							<div class="d-flex col-10 mb-5 ps-4 pe-5">
								<p class="col-3 py-3 fs-5">Price</p>
								<p class="col-10 text-end py-3 fs-5">${{$secondhand->price}}</p>
							</div>
							<div class="d-flex col-10 ps-4 pe-5">
								<p class="col-3 py-3 fs-5">Used for</p>
								<p class="col-10 text-end py-3 fs-5">{{$secondhand->year}} {{Str::plural('year',$secondhand->year)}}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row my-4">
					<div class="col-12 pt-4 ps-4">
						<h4 class="fw-bold">Description</h4>
						<p class="fs-5">{{$secondhand->description}}</p>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="row justify-content-center">
					<div class="row justify-content-center py-4">
						<div class="row">
							<h3 class="col-12 fw-bold mb-4 ms-2">Contact Info.</h3>
						</div>
						<div class="row mb-4">
							<img class="col-12" src="{{asset('imgs/selfie.png')}}" alt="photo">
						</div>
						<div class="row mb-4 align-items-center">
							<h5 class="col-3 my-1">Username</h5>
							<h5 class="col-9 text-end my-1">{{$secondhand->user()->first()->username}}</h5>
						</div>
						<div class="row align-items-center">
							<h5 class="col-3 my-1">Email</h5>
							<h5 class="col-9 text-end my-1">{{$secondhand->user()->first()->email}}</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection