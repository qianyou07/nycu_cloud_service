@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/secondhand.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
	<h2 class="col-11 pt-4 ps-5 mb-4 fw-bold text-end">Second-hand items sales</h2>
	<div class="row justify-content-center">
		<div class="col-11 mb-4 mt-4">
			@foreach ($secondhands as $secondhand)
				<a href="{{route('secondhand.info',$secondhand)}}" class="mb-3 ps-5 py-3 d-flex col-12">
					<img class="col-1" src="{{$secondhand->img}}" alt="house image">
					<h4 class="col-5 ps-5">{{$secondhand->title}}</h4>
					<p class="col-2 ms-auto me-5 text-center d-flex justify-content-center align-items-center fs-3">${{$secondhand->price}}</p>
				</a>
			@endforeach
			<div class="d-flex justify-content-center">
				{!! $secondhands->links() !!}
			</div>
		</div>
	</div>
</div>
@endsection