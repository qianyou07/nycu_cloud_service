@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/roommateInfo.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row justify-content-evenly">
			<div class="col-7">
				<div class="row py-4 px-4">
					<div class="col-5">
						<div id="slide" class="carousel slide" data-bs-ride="carousel">
							<div class="carousel-inner">
								@foreach (json_decode($post->house()->first()->imgs) as $key => $img)
								<div class="carousel-item {{($key ===array_key_first(json_decode($post->house()->first()->imgs)))? "active":""}}">
									<img src="{{$img}}" class="d-block w-100">
								</div>
								@endforeach
							</div>
							<button class="carousel-control-prev" type="button" data-bs-target="#slide" data-bs-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Previous</span>
							</button>
							<button class="carousel-control-next" type="button" data-bs-target="#slide" data-bs-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Next</span>
							</button>
						</div>
					</div>
					<div class="col-7">
						<div class="row mb-4">
							<div class="col-10 d-flex align-items-center justify-content-center">
								<h3 class="text-center">{{$post->house()->first()->title}}</h3>
							</div>
							<a class="col-1 d-flex align-items-center justify-content-center" href="{{route('notification.house',$post->house()->first())}}">Info.</a>
							{{-- <div class="col-2">
							</div> --}}
						</div>
						<div class="row mb-4 my-5 justify-content-center">
							<div class="col-10">
								<p class="text-center py-3 fs-5">{{$post->house()->first()->area}}</p>
							</div>
						</div>
						<div class="row mb-4 justify-content-center">
							<div class="col-10">
								<p class="text-center py-3 fs-5">${{$post->house()->first()->price}}/month</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row my-4">
					<div class="col-12 pt-4 ps-4">
						<h4 class="fw-bold">Description</h4>
						<p class="fs-5">{{$post->description}}</p>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="row justify-content-center">
					<div class="row justify-content-center py-4">
						<div class="row">
							<h3 class="col-12 fw-bold mb-4 ms-2">Contact Info.</h3>
						</div>
						<div class="row mb-4">
							<img class="col-12" src="{{asset('imgs/selfie.png')}}" alt="photo">
						</div>
						<div class="row mb-4 align-items-center">
							<h5 class="col-3 my-3">Username</h5>
							<h5 class="col-9 text-end my-3">{{$post->user()->first()->username}}</h5>
						</div>
						<div class="row align-items-center">
							<h5 class="col-3 my-3">Email</h5>
							<h5 class="col-9 text-end my-3">{{$post->user()->first()->email}}</h5>
						</div>
					</div>
					<div class="row mt-5">
						<h3 class="col text-center">is looking for {{$post->people}} {{Str::plural('roommate',$post->people)}}</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection