@extends('layouts.app')

@section('fonts')
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique+B1&display=swap" rel="stylesheet">
@endsection

@section('styles')
	<link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container mt-5">
		<div class="row align-items-end">
			<div class="col-5 text-end ms-auto mt-5">
				<div class="row">
					<div class="col"><h1 class="display-6">Your best assistant.</h1></div>
				</div>
				<div class="row mt-3">
					<div class="col"><p class="fs-5">Take less time<br>to get information<br>in no time.</p></div>
				</div>
				<div class="row mt-5">
					<div class="col"><a href=" {{route('home')}} " class="text-nowrap shadow-sm">Get started!</a></div>
				</div>
			</div>
		</div>
	</div>
@endsection
